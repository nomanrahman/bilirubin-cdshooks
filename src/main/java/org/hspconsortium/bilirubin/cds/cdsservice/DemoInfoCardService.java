package org.hspconsortium.bilirubin.cds.cdsservice;

import org.hspconsortium.bilirubin.cds.factory.BilirubinCardFactory;
import org.hspconsortium.cdshooks.model.HookCatalog;
import org.hspconsortium.cdshooks.model.ServiceDefinition;
import org.hspconsortium.cdshooks.model.ServiceInvocation;
import org.hspconsortium.cdshooks.model.ServiceResponse;
import org.hspconsortium.cdshooks.service.ExecutableService;
import org.hspconsortium.cdshooks.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemoInfoCardService extends ServiceDefinition implements ExecutableService {

    @Override
    public ServiceResponse execute(ServiceInvocation serviceInvocation) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.addCard(BilirubinCardFactory.getCard(BilirubinCardFactory.CardType.info));
        return serviceResponse;
    }

    @Autowired
    public DemoInfoCardService(ServiceRegistry serviceRegistry) {
        this.setId("demo-info-card")
                .setHook(HookCatalog.PatientView)
                .setTitle("Hello World Info Card")
                .setDescription("Sends a Hello World Info Card");
        serviceRegistry.add(this);
    }

}

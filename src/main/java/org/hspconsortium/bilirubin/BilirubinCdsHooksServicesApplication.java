package org.hspconsortium.bilirubin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.hspconsortium.bilirubin", "org.hspconsortium.cdshooks"})
public class BilirubinCdsHooksServicesApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BilirubinCdsHooksServicesApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BilirubinCdsHooksServicesApplication.class, args);
    }

}
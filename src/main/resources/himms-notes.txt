The data can be refreshed by running:

On hooks.fhir.me
  - java -jar target/hspc-tools.jar org.hspconsortium.DataLoad -in datapack/dstu2/usecase/bilirubincdshooks -url http://hooks.fhir.me:9080

Use:
  - ID-BabyWithoutBilirubinObservation
  - ID-BabyWithBilirubinObservation